#ifndef XCBPP_DRAWER_H
#define XCBPP_DRAWER_H

#include <vector>

#include "xcb_forward.h"

namespace xcbpp {
    class Connection;

    class Drawer {
    public:
        Drawer(Connection &, xcb::Window parent, xcb::Window context, xcb::Mask, uint32_t *values);

        void fill_rectangle(const std::vector<xcb::Rectangle> &rectangles);

    private:
        Connection &connection;
        xcb::GraphicalContext gc;
        xcb::Window window;
    };
}

#endif

#ifndef XCBPP_CONNECTION_H
#define XCBPP_CONNECTION_H

#include <utility>
#include <memory>

#include "xcb_forward.h"

struct c_deleter
{
    void operator()(void *p) { free(p); }
};

template <typename T>
using unique_c_ptr = std::unique_ptr<T, c_deleter>;

template <typename T>
auto unique_c(T *p)
{
    return unique_c_ptr<T>{p, c_deleter{}};
}

namespace xcbpp {
    class Connection {
    public:
        Connection():
            connection{xcb::connect(nullptr, nullptr)}
        {}

        ~Connection()
        {
            xcb::disconnect(connection);
        }

        Connection(Connection &&other):
            connection{nullptr}
        {
            using std::swap;
            swap(other.connection, connection);
        }

        auto map_window(xcb::Window window)
        {
            xcb::map_window(connection, window);
        }

        auto flush()
        {
            xcb::flush(connection);
        }

        auto get_setup()
        {
            return xcb::get_setup(connection);
        }

        auto generate_id()
        {
            return xcb::generate_id(connection);
        }

        auto wait_for_event()
        {
            return unique_c(xcb::wait_for_event(connection));
        }

        auto get()
        {
            return connection;
        }

        template <typename ...Args>
        auto create_gc(Args &&...args)
        {
            xcb::create_gc(connection, std::forward<Args>(args)...);
        }

        template <typename ...Args>
        auto create_window(Args &&...args)
        {
            xcb::create_window(connection, std::forward<Args>(args)...);
        }

        template <typename ...Args>
        auto intern_atom_reply(Args &&...args)
        {
            return unique_c(xcb::intern_atom_reply(connection, std::forward<Args>(args)...));
        }

        template <typename ...Args>
        auto change_property(Args &&...args)
        {
            xcb::change_property(connection, std::forward<Args>(args)...);
        }

        template <typename ...Args>
        auto fill_rectangle(Args &&...args)
        {
            xcb::fill_rectangle(connection, std::forward<Args>(args)...);
        }

    private:
        xcb::Connection *connection;
    };
}

#endif

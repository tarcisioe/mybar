#ifndef XCBPP_XCB_FORWARD_H
#define XCBPP_XCB_FORWARD_H

#include <xcb/xcb.h>
#include <xcb/xcb_atom.h>

namespace xcb {
    using Connection = xcb_connection_t;
    using Window = xcb_window_t;
    using Screen = xcb_screen_t;
    using GenericEvent = xcb_generic_event_t;
    using InternAtomCookie = xcb_intern_atom_cookie_t;
    using Atom = xcb_atom_t;
    using Point = xcb_point_t;
    using Rectangle = xcb_rectangle_t;
    using GraphicalContext = xcb_gcontext_t;
    using Mask = uint32_t;

    constexpr auto connect = xcb_connect;
    constexpr auto get_setup = xcb_get_setup;
    constexpr auto setup_roots_iterator = xcb_setup_roots_iterator;
    constexpr auto generate_id = xcb_generate_id;
    constexpr auto create_window = xcb_create_window;
    constexpr auto map_window = xcb_map_window;
    constexpr auto flush = xcb_flush;
    constexpr auto disconnect = xcb_disconnect;
    constexpr auto change_property = xcb_change_property;
    constexpr auto create_gc = xcb_create_gc;
    constexpr auto intern_atom_reply = xcb_intern_atom_reply;
    constexpr auto wait_for_event = xcb_wait_for_event;
    constexpr auto fill_rectangle = xcb_poly_fill_rectangle;


    const auto COPY_FROM_PARENT = XCB_COPY_FROM_PARENT;

    namespace window_class {
        const auto INPUT_OUTPUT = XCB_WINDOW_CLASS_INPUT_OUTPUT;
    }

    namespace prop_mode {
        const auto REPLACE = XCB_PROP_MODE_REPLACE;
    }

    enum GC {
        FOREGROUND = XCB_GC_FOREGROUND,
        GRAPHICS_EXPOSURES = XCB_GC_GRAPHICS_EXPOSURES,
    };

    namespace cw {
        const auto BACK_PIXEL = XCB_CW_BACK_PIXEL;
        const auto EVENT_MASK = XCB_CW_EVENT_MASK;
    }

    namespace event {
        const auto MASK_EXPOSURE = XCB_EVENT_MASK_EXPOSURE;
    }

    namespace atom {
        const auto ATOM = XCB_ATOM_ATOM;
        const auto STRING = XCB_ATOM_STRING;
    }
}

#endif

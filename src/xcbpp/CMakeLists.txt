cmake_minimum_required(VERSION 3.0)
project(xcbpp CXX)

set(SOURCE
    connection.h
    drawer.h
    drawer.cpp
    xcb_forward.h
    xcbpp.cpp
)

UTILS_ADD_LIBRARY(xcbpp ${SOURCE})

target_link_libraries(xcbpp
    ${LIBXCB_LIBRARIES}
)

target_include_directories(xcbpp PUBLIC
    ${LIBXCB_INCLUDE_DIR}
)

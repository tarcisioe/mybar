#include "connection.h"
#include "drawer.h"

namespace xcbpp {
    Drawer::Drawer(Connection &connection, xcb::Window parent, xcb::Window context, xcb::Mask mask, uint32_t *values):
        connection{connection},
        window{context},
        gc{connection.generate_id()}
    {
        connection.create_gc(gc, parent, mask, values);
    }

    void Drawer::fill_rectangle(const std::vector<xcb::Rectangle> &rectangles)
    {
        xcb::fill_rectangle(connection.get(), window, gc, rectangles.size(), rectangles.data());
    }
}

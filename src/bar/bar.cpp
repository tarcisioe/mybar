#include <unistd.h>

#include <memory>

#include "bar.h"
#include "meta_string.h"
#include "functional.h"

using namespace xcb;

namespace bar {
    Bar::Bar(Pixels height):
        connection{},
        screen{setup_roots_iterator(connection.get_setup()).data},
        window{connection.generate_id()},
        black{{ screen->black_pixel }},
        foreground{connection, screen->root, window, xcb::GC::FOREGROUND, black.data()},
        height{height},
        width{screen->width_in_pixels}
    {
        auto mask = xcb::cw::BACK_PIXEL | xcb::cw::EVENT_MASK;
        uint32_t values[] = { screen->white_pixel, xcb::event::MASK_EXPOSURE };

        connection.create_window(
                COPY_FROM_PARENT,
                window,
                screen->root,
                0, screen->height_in_pixels - height,
                screen->width_in_pixels, height,
                0,
                window_class::INPUT_OUTPUT,
                screen->root_visual,
                mask, values
                );

        set_atoms();
    }

    void Bar::show()
    {
        connection.map_window(window);

        connection.flush();

        wait_on_events();
    }

    void Bar::wait_on_events()
    {
        unique_c_ptr<xcb::GenericEvent> e;
        while ((e = connection.wait_for_event())) {
            process(*e);
        }
    }

    void Bar::process(const xcb::GenericEvent &event)
    {
        foreground.fill_rectangle({{0, 0, width, height}});
        connection.flush();
    }

    void Bar::set_cookies(std::array<xcb::InternAtomCookie, N_ATOMS> &cookies)
    {
        cookies[0] = cookie(meta::make_string("_NET_WM_WINDOW_TYPE"));
        cookies[1] = cookie(meta::make_string("_NET_WM_WINDOW_TYPE_DOCK"));
    }

    void Bar::set_atoms()
    {
        auto cookies = std::array<xcb::InternAtomCookie, N_ATOMS>{};
        auto atoms = std::array<xcb::Atom, N_ATOMS>{};

        set_cookies(cookies);

        auto failed = false;

        for_enum(cookies, [&](auto i, auto cookie)
                {
                auto reply = connection.intern_atom_reply(cookie, nullptr);
                if (!reply) { failed = true; return; }
                atoms[i] = reply->atom;
                });

        if (failed) { return; }

        using namespace xcb;
        connection.change_property(
                prop_mode::REPLACE,
                window,
                atoms[0],
                atom::ATOM,
                32,
                1,
                &atoms[1]
                );
    }
}

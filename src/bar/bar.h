#ifndef BAR_BAR_H
#define BAR_BAR_H

#include <array>

#include "connection.h"
#include "drawer.h"
#include "xcb_forward.h"

namespace bar {

using Pixels = uint16_t;

class Bar {
public:
    Bar(Pixels height=30);

    void show();

private:
    static constexpr auto N_ATOMS = 2;

    void set_atoms();
    void init_foreground();
    void process(const xcb::GenericEvent &);
    void set_cookies(std::array<xcb_intern_atom_cookie_t, N_ATOMS> &);
    void wait_on_events();

    template <typename T>
    xcb_intern_atom_cookie_t cookie(T str)
    {
        return xcb_intern_atom(connection.get(), 0, str.size(), str.c_str());
    }

    xcbpp::Connection connection;
    xcb::Window window;
    xcb::Screen *screen;
    std::array<uint32_t, 2> black;
    xcbpp::Drawer foreground;
    Pixels height;
    Pixels width;
};

}

#endif

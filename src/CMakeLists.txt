cmake_minimum_required(VERSION 3.0)
project(mybar CXX)

set(CMAKE_MODULE_PATH
    "${CMAKE_SOURCE_DIR}/cmake"
    "${CMAKE_SOURCE_DIR}/cmake-utils")

include(utils)

enable_testing()

if(PROFILE)
    if(CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "-pg")
        set(CMAKE_C_FLAGS "-pg")
        set(CMAKE_EXE_FLAGS "-pg")
    endif()
endif()


if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -flto")
endif()

find_package(XCB)

add_subdirectory(utils)
add_subdirectory(xcbpp)
add_subdirectory(bar)
add_subdirectory(app)

foreach(_target ${TARGETS})
    set_property(TARGET ${_target} PROPERTY CXX_STANDARD 14)
endforeach()

UTILS_GENERATE_CLANG_COMPLETE()

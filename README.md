My Unnamed Bar
--------------

This is a status bar with support for i3. It does nothing amazing yet. It is
very redundant with lots of other projects. It is being made for fun only.

It uses C++ and builds with CMake, in a straightforward way:

```
mkdir build
cd build
cmake ../src
make
```

Before building, don't forget to `init` and `update` the submodules.

Look at the output of `cmake -L` for options when building.

To generate `.clang_complete` files for use with stuff such as Syntastic or
clang\_complete, use the `-DUTILS_CLANG_COMPLETE=ON` flag.
